<?php
namespace edu\wisc\doit\uwphps;

trait EnvironmentHelper
{
    use HttpHeaderMapper;

    /**
     * Sets environment variables
     *
     * @param array $environment  pairs of environment variable names and values
     */
    private function setEnvironment(array $environment)
    {
        array_walk($environment, function ($value, $key) {
            putenv("$key=$value");
        });
    }

    /**
     * Removes the given environment variable from the environment
     *
     * @param string $variable  name of environment variable
     */
    private function removeEnvironmentVariable($variable)
    {
        putenv($variable);
    }

    /**
     * Converts environment variable names to their HTTP header equivalents
     *
     * @param array $environment
     * @return array  environment with variable names converted to HTTP headers
     * @see HttpHeaderMapper::httpHeaderFromAttribute()
     */
    private function toHttpHeaders(array $environment)
    {
        foreach ($environment as $key => $value) {
            $environment[$this->httpHeaderFromAttribute($key)] = $value;
            unset($environment[$key]);
        }
        return $environment;
    }
}
