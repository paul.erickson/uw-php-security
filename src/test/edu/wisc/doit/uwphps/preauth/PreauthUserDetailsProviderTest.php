<?php
namespace edu\wisc\doit\uwphps\preauth;

use edu\wisc\doit\uwphps\EnvironmentHelper;
use edu\wisc\doit\uwphps\UserDetailsProvider;

/**
 * Tests for {@link PreauthUserDetailsProvider}
 */
class PreauthUserDetailsProviderTest extends \PHPUnit_Framework_TestCase
{

    use EnvironmentHelper;

    /** @var PreauthUserDetailsProvider */
    private $provider;

    /** @var PreauthUserDetailsProvider */
    private $providerWithHttp;

    /** @var array  environment variables to be set */
    private $environment;

    protected function setUp()
    {
        $this->provider = new PreauthUserDetailsProvider(false);
        $this->providerWithHttp = new PreauthUserDetailsProvider(true);

        // Default environment variables
        $this->environment = [
            PreauthUserDetailsProvider::FULL_NAME => 'BUCKINGHAM BADGER',
            PreauthUserDetailsProvider::EPPN => 'bbadger@wisc.edu',
            PreauthUserDetailsProvider::FIRST_NAME => 'BUCKINGHAM',
            PreauthUserDetailsProvider::EMAIL => 'bucky.badger@wisc.edu',
            PreauthUserDetailsProvider::SHIB_SESSION_ID => '1234567890',
            PreauthUserDetailsProvider::LAST_NAME => 'BADGER',
            PreauthUserDetailsProvider::SOURCE => 'a_source',
            PreauthUserDetailsProvider::PVI => 'UW123A456',
            PreauthUserDetailsProvider::ISIS_EMPLID => '123456789',
            PreauthUserDetailsProvider::MEMBER_OF => 'A06;A07'
        ];
    }

    /** @test */
    public function loadsNullUserWhenNoSession()
    {
        // Ensure the Shib Session ID environment variables are deleted
        $this->removeEnvironmentVariable(UserDetailsProvider::SHIB_SESSION_ID);
        $this->removeEnvironmentVariable(UserDetailsProvider::SHIB_SESSION_ID_HTTP);
        static::assertNull($this->provider->loadUser());
        static::assertNull($this->providerWithHttp->loadUser());
    }

    /** @test */
    public function loadsUser()
    {
        $this->setEnvironment($this->environment);
        $user = $this->provider->loadUser();

        static::assertNotNull($user);
        static::assertEquals($this->environment[PreauthUserDetailsProvider::EPPN], $user->getEppn());
        static::assertEquals($this->environment[PreauthUserDetailsProvider::PVI], $user->getPvi());
        static::assertEquals($this->environment[PreauthUserDetailsProvider::FULL_NAME], $user->getFullName());
        static::assertEquals($this->environment[PreauthUserDetailsProvider::EMAIL], $user->getEmailAddress());
        static::assertEquals($this->environment[PreauthUserDetailsProvider::SOURCE], $user->getSource());
        static::assertEquals($this->environment[PreauthUserDetailsProvider::ISIS_EMPLID], $user->getIsisEmplid());
        static::assertEquals($this->environment[PreauthUserDetailsProvider::FIRST_NAME], $user->getFirstName());
        static::assertEquals($this->environment[PreauthUserDetailsProvider::LAST_NAME], $user->getLastName());
        static::assertEquals(
            explode(PreauthUserDetailsProvider::DELIMITER, $this->environment[PreauthUserDetailsProvider::MEMBER_OF]),
            $user->getIsMemberOf()
        );
    }

    /** @test */
    public function loadsUserWithHttpHeaders()
    {
        $this->setEnvironment($this->toHttpHeaders($this->environment));
        $user = $this->providerWithHttp->loadUser();

        static::assertNotNull($user);
        static::assertEquals($this->environment[PreauthUserDetailsProvider::EPPN], $user->getEppn());
        static::assertEquals($this->environment[PreauthUserDetailsProvider::PVI], $user->getPvi());
        static::assertEquals($this->environment[PreauthUserDetailsProvider::FULL_NAME], $user->getFullName());
        static::assertEquals($this->environment[PreauthUserDetailsProvider::EMAIL], $user->getEmailAddress());
        static::assertEquals($this->environment[PreauthUserDetailsProvider::SOURCE], $user->getSource());
        static::assertEquals($this->environment[PreauthUserDetailsProvider::ISIS_EMPLID], $user->getIsisEmplid());
        static::assertEquals($this->environment[PreauthUserDetailsProvider::FIRST_NAME], $user->getFirstName());
        static::assertEquals($this->environment[PreauthUserDetailsProvider::LAST_NAME], $user->getLastName());
        static::assertEquals(
            explode(PreauthUserDetailsProvider::DELIMITER, $this->environment[PreauthUserDetailsProvider::MEMBER_OF]),
            $user->getIsMemberOf()
        );
    }

    /** @test */
    public function missingAttributeIsFalse()
    {
        $this->setEnvironment($this->environment);
        $this->removeEnvironmentVariable(PreauthUserDetailsProvider::ISIS_EMPLID);
        $user = $this->provider->loadUser();
        static::assertNotNull($user);
        static::assertFalse($user->getIsisEmplid());
    }
}
