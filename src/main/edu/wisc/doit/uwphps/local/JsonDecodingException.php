<?php
namespace edu\wisc\doit\uwphps\local;

/**
 * Exception when JSON cannot be decoded
 */
class JsonDecodingException extends \RuntimeException
{
}