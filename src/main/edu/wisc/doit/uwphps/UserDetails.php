<?php

namespace edu\wisc\doit\uwphps;

/**
 * UserDetails defines a minimal representation of a user associated with UW.
 */
interface UserDetails
{
    /**
     * EPPN is the scoped username and typically (but not always) takes the form 'netid@wisc.edu'. This method will return
     * null if the {@link UserDetailsService} backing it is incapable of providing an EPPN.
     * 
     * @return string|null
     */
    public function getEppn();

    /**
     * PVI stands for "publicly visible identifier" and is intended to serve as the entity identifier for a user. This attribute
     * is the preferred choice for unique identifier. While it can change for a given individual, it is the least likely of the
     * identifying attributes to change and only changes in extreme edge cases.
     * 
     * @return string
     */
    public function getPvi();

    /**
     * The full name for this user, or null if one is not available.
     * 
     * @return string|null
     */
    public function getFullName();

    /**
     * A never null, but possibly empty, array of strings each representing the UDDS IDs of the group this user is a member of.
     * 
     * @return array
     */
    public function getUddsMembership();

    /**
     * The user's email address. Prefer lowercase.
     * 
     * @return string|null
     */
    public function getEmailAddress();

    /**
     * ID representing the source system that provided this user. Implementations of {@link UserDetailsService} will typically
     * populate this field for the the concrete {@link UserDetails} returned.
     * 
     * @return string
     */
    public function getSource();

    /**
     * 'ISIS Emplid' is a unique identifier for a person within the Peoplesoft student information system.
     * 
     * @return string|null
     */
    public function getIsisEmplid();

    /**
     * The first name of the user.
     * 
     * @return string|null
     */
    public function getFirstName();

    /**
     * The last name of the user.
     * 
     * @return string|null
     */
    public function getLastName();

    /**
     * The user's Manifest group memberships delivered to the application.
     *
     * @return array|null
     */
    public function getIsMemberOf();
    
}
