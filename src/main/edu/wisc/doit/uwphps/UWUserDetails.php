<?php

namespace edu\wisc\doit\uwphps;

/**
 * Default, concrete implementation of {@link UserDetails} to represent a UW user.
 */
class UWUserDetails implements UserDetails
{

    /** @var string */
    private $eppn;
    /** @var string */
    private $pvi;
    /** @var string */
    private $fullName;
    /** @var array */
    private $udds;
    /** @var string */
    private $emailAddress;
    /** @var string */
    private $source;
    /** @var string */
    private $isisEmplid;
    /** @var string */
    private $firstName;
    /** @var string */
    private $lastName;
    /** @var array */
    private $memberships;

    /**
     * UWUserDetails constructor.
     * @param string $eppn
     * @param string $pvi
     * @param string $fullName
     * @param array $udds
     * @param string $emailAddress
     * @param string $source
     * @param string $isisEmplid
     * @param string $firstName
     * @param string $lastName
     */
    public function __construct(
        $eppn,
        $pvi,
        $fullName,
        $udds,
        $emailAddress,
        $source,
        $isisEmplid,
        $firstName,
        $lastName,
        $memberships
    ) {
        $this->eppn = $eppn;
        $this->pvi = $pvi;
        $this->fullName = $fullName;
        $this->udds = $udds;
        $this->emailAddress = $emailAddress;
        $this->source = $source;
        $this->isisEmplid = $isisEmplid;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->memberships = $memberships;
    }

    /**
     * {@inheritdoc}
     */
    public function getEppn()
    {
        return $this->eppn;
    }

    /**
     * {@inheritdoc}
     */
    public function getPvi()
    {
        return $this->pvi;
    }

    /**
     * {@inheritdoc}
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * {@inheritdoc}
     */
    public function getUddsMembership()
    {
        return $this->udds;
    }

    /**
     * {@inheritdoc}
     */
    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    /**
     * {@inheritdoc}
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * {@inheritdoc}
     */
    public function getIsisEmplid()
    {
        return $this->isisEmplid;
    }

    /**
     * {@inheritdoc}
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * {@inheritdoc}
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * {@inheritdoc}
     */
    public function getIsMemberOf()
    {
        return $this->memberships;
    }

}