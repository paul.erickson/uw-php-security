# uw-php-security

uw-php-security is a PHP companion to [uw-spring-security](https://git.doit.wisc.edu/adi-ia/uw-spring-security) for Java. Its purpose is to ease development of PHP applications needing details about UW users provided through Shibboleth.

Like uw-spring-security, uw-php-security provides a class called `UWUserDetails` for easily accessing common UW user attributes. This model is provided to applications through a `UserDetailsProvider`, and uw-php-security provides implementations suitable for both local and preauth (Shibboleth) environments.

```php
<?php

use edu\wisc\doit\PreauthUserDetailsProvider; // or LocalUserDetailsProvider for local development
...
$userDetailsProvider = new PreauthUserDetailsProvider();
$user = $userDetailsProvider->loadUser();
if ($user == null) {
    // handle error
}
...
```

## Developer Requirements

* PHP
* [Composer](https://getcomposer.org/)
* [Phing](https://www.phing.info/)
* [PHPUnit](https://phpunit.de/)

## Building

To build the project and execute tests, run the following command at the root of the project:

```
php phing.phar
```